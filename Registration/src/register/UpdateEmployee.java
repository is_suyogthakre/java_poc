package register;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UpdateEmployee {

	public List<User> Update(List<User> user) {
		List<User> emp = new ArrayList<>();

		System.out.println("Please Enter the name of Employee you wish to update");
		Scanner scanner = new Scanner(System.in);
		String Name = scanner.nextLine();

		for (User st : user) {
			if (st.Name.equals(Name)) {
				System.out.println("Enter the First Name:");
				String fName = scanner.nextLine();
				if (fName != " ")
					st.Name = fName;

				System.out.println("Enter the EmailId:");
				String EmailId = scanner.nextLine();
				if (!EmailId.isEmpty())
					st.EmailId = EmailId;

				System.out.println("Enter the Address:");
				String Address = scanner.nextLine();
				if (!Address.isEmpty())
					st.Address = Address;

				System.out.println("Enter the DateOfBirth:");
				String DateOfBirth = scanner.nextLine();
				if (!DateOfBirth.isEmpty())
					st.DateOfBirth = DateOfBirth;

				System.out.println("Enter the Age:");
				String Age = scanner.nextLine();
				if (!Age.isEmpty())
					st.Age = Age;

			}
		}
		
		for (User str : user) {
			System.out.println("The Remaining Employee is: " + "\n" + "Name : " + str.Name + "\t" + "EmailId:"
					+ str.EmailId + "\t" + "Age: " + str.Age + "\t" + "Address: " + str.Address + "\t"
					+ "DateOfBirth: " + str.DateOfBirth );
		}

		return emp;
	}
}
