package register;

import java.util.*;

public class NameComparator implements Comparator {

	public int compare(Object o1, Object o2) {
		User s1 = (User) o1;
		User s2 = (User) o2;

		return s1.Name.compareTo(s2.Name);
	}
}
