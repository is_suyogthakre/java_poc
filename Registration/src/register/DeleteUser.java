package register;

import java.util.List;
import java.util.Scanner;

public class DeleteUser {

	public List<User> DeleteEmployee(List<User> user) {

		System.out.println("Please Enter the name you wish to Delete");
		Scanner scanner = new Scanner(System.in);
		String Name = scanner.nextLine();

		for (User st : user) {
			if (st.Name.equals(Name)) {
				user.remove(st);
			}
		}

		if (user != null) {
			for (User st : user) {
				System.out.println("The Remaining Employee is: " + "\n" + "Name : " + st.Name + "\t" + "EmailId:"
						+ st.EmailId + "\t" + "Age: " + st.Age + "\t" + "Address: " + st.Address + "\t"
						+ "DateOfBirth: " + st.DateOfBirth);
			}
		} else {
			System.out.println("User List is Empty");
		}
		return user;

	}
}
