package register;

import java.util.Comparator;

public class EmailIdComparator implements Comparator{
	public int compare(Object o3, Object o4) {
		User s3 = (User) o3;
		User s4 = (User) o4;

		return s3.EmailId.compareTo(s4.EmailId);
	}

}
