package register;

import java.util.Date;

public class User {

	int id;
	String Name;
	String EmailId;
	String Address;
	String Age;
	String DateOfBirth;
	Date CreatedDate;
	Date UpdatedDate;

	public User() {
	};

	public User(int id, String Name, String EmailId, String Address, String Age,
			String DateOfBirth, Date CreatedDate,
			Date UpdatedDate) {
		super();
		this.id = id;
		this.Name = Name;
		this.EmailId = EmailId;
		this.Address = Address;
		this.Age = Age;
		this.DateOfBirth = DateOfBirth;
		this.CreatedDate = CreatedDate;
		this.UpdatedDate = UpdatedDate;

	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getAge() {
		return Age;
	}

	public void setAge(String age) {
		Age = age;
	}

	public String getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public Date getUpdatedDate() {
		return UpdatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		UpdatedDate = updatedDate;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", Name=" + Name + ", EmailId=" + EmailId + ", Address=" + Address + ", Age=" + Age
				+ ", DateOfBirth=" + DateOfBirth + ", CreatedDate=" + CreatedDate + ", UpdatedDate=" + UpdatedDate
				+ "]";
	}


}
