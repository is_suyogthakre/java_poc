package register;

import java.util.Comparator;

public class AgeComparator implements Comparator {
	public int compare(Object o5, Object o6) {
		User s5 = (User) o5;
		User s6 = (User) o6;

		return s5.Age.compareTo(s6.Age);
	}
}
