package register;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InsertUser {

	public List<User> InsertEmployee() {
		List<User> emp = new ArrayList<>();

		boolean input = true;

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dateobj = new Date();

		while (input) {
			Scanner sc = new Scanner(System.in);

			System.out.println("Welcome to employee reg!!");

			System.out.println("Enter the First Name:");
			String Name = sc.nextLine();

			System.out.println("Enter the EmailId:");
			String EmailId = sc.nextLine();

			System.out.println("Enter the Address:");
			String Address = sc.nextLine();

			System.out.println("Enter the DateOfBirth:");
			String DateOfBirth = sc.nextLine();

			System.out.println("Enter the Age:");
			String Age = sc.nextLine();
			while (isValidAge(Age)) {
				System.out.println("Age cannot be greater than 60. Please enter again:");
				Age = sc.nextLine();
			}

			User employee = new User();
			employee.setName(Name);
			employee.setEmailId(EmailId);
			employee.setAddress(Address);
			employee.setDateOfBirth(DateOfBirth);

			employee.setAge(Age);

			emp.add(employee);

			Sorting(emp);
			Iterator itr = emp.iterator();

			while (itr.hasNext()) {
				User st = (User) itr.next();

				System.out.println("The Reg Employee is: " + "\n" + "Name : " + st.getName() + "\t" + "EmailId:"
						+ st.getEmailId() + "\t" + "Age: " + st.getAge() + "\t" + "Address: " + st.getAddress() + "\t"
						+ "DateOfBirth: " + st.getDateOfBirth() + "\t" + "Created Date: " + df.format(dateobj));

			}

			System.out.println("Do you wish to continue adding employee? (y/n):");
			String in = sc.nextLine();
			if (in.equals("n")) {
				input = false;
				System.out.println("Employee reg complete!!:");
			}

		}

		return emp;
	}

	private void Sorting(List<User> emp) {
		Collections.sort(emp,
				new NameComparator().thenComparing(new EmailIdComparator()).thenComparing(new AgeComparator()));
	}

	public static boolean isValidAge(String age) {
		String regex = "^([7-9]\\d|[1-9]\\d{2,})$";
		Pattern p = Pattern.compile(regex);
		if (age == null) {
			return false;
		}
		Matcher m = p.matcher(age);
		return m.matches();
	}

}
