package register;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class EmployeeReg {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<User> user = new ArrayList<>();
		int num = 0;

		boolean input = true;

		while (input) {
			System.out.print("Enter 1 for Insert || Enter 2 for Update || Enter 3 for Delete || Enter 4 for Search ");
			Scanner scanner = new Scanner(System.in);
			num = scanner.nextInt();
			if (num == 1) { // Insert

				InsertUser insert = new InsertUser();
				user = insert.InsertEmployee();

			} else if (num == 2) { // update

				UpdateEmployee update = new UpdateEmployee();
				user = update.Update(user);

			} else if (num == 3) {// Delete

				DeleteUser delete = new DeleteUser();
				user = delete.DeleteEmployee(user);
				
			} else if (num == 4) {// Search

				System.out.print("starting search ");
				SearchUser search = new SearchUser();
				user = search.Search(user);

			} else {
				
				input = false;
			}

		}
	}

}
