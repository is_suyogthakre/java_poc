package register;

import java.util.List;
import java.util.Scanner;

public class SearchUser {
	public List<User> Search(List<User> user) {

		System.out.println("Please Enter the name of Employee you wish to Search");
		Scanner scanner = new Scanner(System.in);
		String Name = scanner.nextLine();

		for (User st : user) {
	        if (st.Name.equals(Name) || st.EmailId.equals(Name)) {
	        	System.out.println("The Searched Employee is: " + "\n" + "Name : " + st.Name + "\t" + "EmailId:"
						+ st.EmailId + "\t" + "Age: " + st.Age + "\t" + "Address: " + st.Address + "\t"
						+ "DateOfBirth: " + st.DateOfBirth);
	        }
	    }

		return user;
	}
}
